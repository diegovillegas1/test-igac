package com.igac.diegovillegas.test_igac.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.igac.diegovillegas.test_igac.models.OrigenOferta;

@Service
public interface OrigenOfertaService {

    List<OrigenOferta> findAll();
    
}
