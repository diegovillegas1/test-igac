package com.igac.diegovillegas.test_igac.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igac.diegovillegas.test_igac.models.CondicionJuridica;
import com.igac.diegovillegas.test_igac.repositories.CondicionJuridicaRepository;

@Service
public class CondicionJuridicaServiceImp implements CondicionJuridicaService {

    @Autowired
    private CondicionJuridicaRepository repository;

    @Override
    public List<CondicionJuridica> findAll() {
        return (List<CondicionJuridica>) repository.findAll();
    }

}
