package com.igac.diegovillegas.test_igac.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igac.diegovillegas.test_igac.models.Oferta;
import com.igac.diegovillegas.test_igac.repositories.OfertaRepository;

@Service
public class OfertaServiceImpl implements OfertaService {

    @Autowired
    private OfertaRepository repository;

    @Override
    public List<Oferta> findAll() {
        return (List<Oferta>) repository.findAll();
    }

    @Override
    public Optional<Oferta> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Oferta save(Oferta Oferta) {
        return repository.save(Oferta);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

}
