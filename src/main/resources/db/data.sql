INSERT INTO public.condicion_juridica (condicion) VALUES('NPH');
INSERT INTO public.condicion_juridica (condicion) VALUES('PH');
INSERT INTO public.condicion_juridica (condicion) VALUES('MATRIZ');
INSERT INTO public.condicion_juridica (condicion) VALUES('Unidad predial');
INSERT INTO public.condicion_juridica (condicion) VALUES('Condominio');
INSERT INTO public.condicion_juridica (condicion) VALUES('Condominio matriz');
INSERT INTO public.condicion_juridica (condicion) VALUES('Condominio unidad predial parque cementerio matriz');
INSERT INTO public.condicion_juridica (condicion) VALUES('Parque cementerio unidad');
INSERT INTO public.condicion_juridica (condicion) VALUES('Vía');
INSERT INTO public.condicion_juridica (condicion) VALUES('Informal');
INSERT INTO public.condicion_juridica (condicion) VALUES('Bien uso público');

INSERT INTO public.tipo_oferta (tipo) VALUES('Venta');
INSERT INTO public.tipo_oferta (tipo) VALUES('Arriendo');

INSERT INTO public.tipo_predio (tipo) VALUES('Urbano');
INSERT INTO public.tipo_predio (tipo) VALUES('Rural');

INSERT INTO public.origen_oferta (origen) VALUES('IVP');
INSERT INTO public.origen_oferta (origen) VALUES('Actualización');
INSERT INTO public.origen_oferta (origen) VALUES('Conservación');
INSERT INTO public.origen_oferta (origen) VALUES('Formación');
INSERT INTO public.origen_oferta (origen) VALUES('Investigación');

