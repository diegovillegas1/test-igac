package com.igac.diegovillegas.test_igac.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.igac.diegovillegas.test_igac.models.TipoOferta;
import com.igac.diegovillegas.test_igac.services.TipoOfertaService;

@RestController
@RequestMapping("/api/tipos-oferta")

public class TiposOfertaController {

    @Autowired
    private TipoOfertaService service;

    @GetMapping("")
    public List<TipoOferta> list() {
        return service.findAll();
    }
}
