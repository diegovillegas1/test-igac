package com.igac.diegovillegas.test_igac.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.igac.diegovillegas.test_igac.models.OrigenOferta;
import com.igac.diegovillegas.test_igac.services.OrigenOfertaService;

@RestController
@RequestMapping("/api/origenes-oferta")
public class OrigenOfertaController {

    @Autowired
    private OrigenOfertaService service;

    @GetMapping("")
    public List<OrigenOferta> list() {
        return service.findAll();
    }
}
