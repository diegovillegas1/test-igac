package com.igac.diegovillegas.test_igac.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.igac.diegovillegas.test_igac.models.TipoPredio;
import com.igac.diegovillegas.test_igac.services.TipoPredioService;

@RestController
@RequestMapping("/api/tipos-predio")
public class TiposPredioController {
    @Autowired
    private TipoPredioService service;

    @GetMapping("")
    public List<TipoPredio> list() {
        return service.findAll();
    }
}
