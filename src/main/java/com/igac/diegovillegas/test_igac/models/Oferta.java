package com.igac.diegovillegas.test_igac.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "oferta")
public class Oferta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero_predial_nuevo", length = 30)
    private String numeroPredialNuevo;

    @Column(name = "numero_predial_antiguo", length = 20)
    private String numeroPredialAntiguo;

    @Column(name = "codigo_homologado", length = 20)
    private String codigoHomologado;

    @Column(name = "matricula_inmobiliaria", length = 20)
    private String matriculaInmobiliaria;

    @ManyToOne
    @JoinColumn(name = "condicion_juridica")
    private CondicionJuridica condicionJuridica;

    @ManyToOne
    @JoinColumn(name = "tipo_oferta")
    private TipoOferta tipoOferta;

    @ManyToOne
    @JoinColumn(name = "tipo_predio")
    private TipoPredio tipoPredio;

    @ManyToOne
    @JoinColumn(name = "origen_oferta")
    private OrigenOferta origenOferta;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroPredialNuevo() {
        return numeroPredialNuevo;
    }

    public void setNumeroPredialNuevo(String numeroPredialNuevo) {
        this.numeroPredialNuevo = numeroPredialNuevo;
    }

    public String getNumeroPredialAntiguo() {
        return numeroPredialAntiguo;
    }

    public void setNumeroPredialAntiguo(String numeroPredialAntiguo) {
        this.numeroPredialAntiguo = numeroPredialAntiguo;
    }

    public String getCodigoHomologado() {
        return codigoHomologado;
    }

    public void setCodigoHomologado(String codigoHomologado) {
        this.codigoHomologado = codigoHomologado;
    }

    public String getMatriculaInmobiliaria() {
        return matriculaInmobiliaria;
    }

    public void setMatriculaInmobiliaria(String matriculaInmobiliaria) {
        this.matriculaInmobiliaria = matriculaInmobiliaria;
    }

    public CondicionJuridica getCondicionJuridica() {
        return condicionJuridica;
    }

    public void setCondicionJuridica(CondicionJuridica condicionJuridica) {
        this.condicionJuridica = condicionJuridica;
    }

    public TipoOferta getTipoOferta() {
        return tipoOferta;
    }

    public void setTipoOferta(TipoOferta tipoOferta) {
        this.tipoOferta = tipoOferta;
    }

    public TipoPredio getTipoPredio() {
        return tipoPredio;
    }

    public void setTipoPredio(TipoPredio tipoPredio) {
        this.tipoPredio = tipoPredio;
    }

    public OrigenOferta getOrigenOferta() {
        return origenOferta;
    }

    public void setOrigenOferta(OrigenOferta origenOferta) {
        this.origenOferta = origenOferta;
    }
}
