package com.igac.diegovillegas.test_igac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestIgacApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestIgacApplication.class, args);
	}

}
