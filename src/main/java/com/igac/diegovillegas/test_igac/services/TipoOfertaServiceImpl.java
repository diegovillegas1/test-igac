package com.igac.diegovillegas.test_igac.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igac.diegovillegas.test_igac.models.TipoOferta;
import com.igac.diegovillegas.test_igac.repositories.TipoOfertaRepository;

@Service
public class TipoOfertaServiceImpl implements TipoOfertaService {

    @Autowired
    private TipoOfertaRepository repository;

    @Override
    public List<TipoOferta> findAll() {
        return (List<TipoOferta>) repository.findAll();
    }

}
