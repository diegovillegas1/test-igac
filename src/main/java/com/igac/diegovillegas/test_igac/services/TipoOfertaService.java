package com.igac.diegovillegas.test_igac.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.igac.diegovillegas.test_igac.models.TipoOferta;

@Service
public interface TipoOfertaService {
    List<TipoOferta> findAll();
}
