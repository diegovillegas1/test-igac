# Prueba desarrollador IGAC

## Configuración del Archivo `application.properties`

Para que la aplicación se conecte a la base de datos local, debes modificar los datos de conexión en el archivo `application.properties` ubicado en el directorio `src/main/resources`.


## Creación de estructura y carga de datos básicos

Para esto se debe ejecutar los archivos `src/main/resources/db/structure.sql` y `src/main/resources/db/data.sql`


## Colecciones Postman

Para facilitar las pruebas de los servicios se incluyen las colecciones `src/main/resources/postman/Catalogos.postman_collection.json` y `src/main/resources/postman/Ofertas.postman_collection.json`
