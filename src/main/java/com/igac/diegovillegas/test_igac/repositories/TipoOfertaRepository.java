package com.igac.diegovillegas.test_igac.repositories;

import org.springframework.data.repository.CrudRepository;

import com.igac.diegovillegas.test_igac.models.TipoOferta;

public interface TipoOfertaRepository extends CrudRepository<TipoOferta, Long> {

}
