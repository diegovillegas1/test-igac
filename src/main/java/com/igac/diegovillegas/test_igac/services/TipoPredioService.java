package com.igac.diegovillegas.test_igac.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.igac.diegovillegas.test_igac.models.TipoPredio;

@Service
public interface TipoPredioService {
    List<TipoPredio> findAll();
}
