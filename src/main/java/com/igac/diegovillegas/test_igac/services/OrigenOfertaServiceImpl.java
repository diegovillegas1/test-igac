package com.igac.diegovillegas.test_igac.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igac.diegovillegas.test_igac.models.OrigenOferta;
import com.igac.diegovillegas.test_igac.repositories.OrigenOfertaRepository;

@Service
public class OrigenOfertaServiceImpl implements OrigenOfertaService {

    @Autowired
    private OrigenOfertaRepository repository;

    @Override
    public List<OrigenOferta> findAll() {
        return (List<OrigenOferta>) repository.findAll();
    }

}
