package com.igac.diegovillegas.test_igac.repositories;

import org.springframework.data.repository.CrudRepository;

import com.igac.diegovillegas.test_igac.models.OrigenOferta;

public interface OrigenOfertaRepository extends CrudRepository<OrigenOferta, Long> {

}
