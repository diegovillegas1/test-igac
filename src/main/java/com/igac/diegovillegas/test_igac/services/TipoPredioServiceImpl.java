package com.igac.diegovillegas.test_igac.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igac.diegovillegas.test_igac.models.TipoPredio;
import com.igac.diegovillegas.test_igac.repositories.TipoPredioRepository;

@Service
public class TipoPredioServiceImpl implements TipoPredioService {

    @Autowired
    private TipoPredioRepository repository;

    @Override
    public List<TipoPredio> findAll() {
        return (List<TipoPredio>) repository.findAll();
    }
}
