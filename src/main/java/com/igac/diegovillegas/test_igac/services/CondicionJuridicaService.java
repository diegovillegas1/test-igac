package com.igac.diegovillegas.test_igac.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.igac.diegovillegas.test_igac.models.CondicionJuridica;

@Service
public interface CondicionJuridicaService {

    List<CondicionJuridica> findAll();

    /*
    Optional<CondicionJuridica> findById(Long id);
    CondicionJuridica save(CondicionJuridica condicionJuridica);
    void deleteById(Long id);
     */
}

