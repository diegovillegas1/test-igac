package com.igac.diegovillegas.test_igac.repositories;

import org.springframework.data.repository.CrudRepository;

import com.igac.diegovillegas.test_igac.models.CondicionJuridica;

public interface CondicionJuridicaRepository extends CrudRepository<CondicionJuridica, Long> {

}
