package com.igac.diegovillegas.test_igac.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.igac.diegovillegas.test_igac.models.Oferta;

@Service
public interface OfertaService {

    List<Oferta> findAll();
    Optional<Oferta> findById(Long id);
    Oferta save(Oferta Oferta);
    void deleteById(Long id);
}
