package com.igac.diegovillegas.test_igac.repositories;

import org.springframework.data.repository.CrudRepository;

import com.igac.diegovillegas.test_igac.models.TipoPredio;

public interface TipoPredioRepository extends CrudRepository<TipoPredio, Long> {

}
