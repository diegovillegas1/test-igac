package com.igac.diegovillegas.test_igac.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.igac.diegovillegas.test_igac.models.Oferta;
import com.igac.diegovillegas.test_igac.services.OfertaService;

@RestController
@RequestMapping("/api/ofertas")

public class OfertaController {
    @Autowired
    private OfertaService service;

    @GetMapping("")
    public List<Oferta> list() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> view(@PathVariable Long id) {
        Optional<Oferta> oferta = service.findById(id);
        if(oferta.isPresent())
        {
            return ResponseEntity.ok(oferta.get());
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    
    @PostMapping("")
    public ResponseEntity<Oferta> create(@RequestBody Oferta oferta) {
        Oferta nuevaOferta = service.save(oferta);
        return ResponseEntity.status(HttpStatus.CREATED).body(nuevaOferta);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Oferta> update(@PathVariable Long id, @RequestBody Oferta oferta) {
        Oferta anteriorOferta = service.findById(id).orElse(null);

        anteriorOferta.setNumeroPredialNuevo(oferta.getNumeroPredialNuevo());
        anteriorOferta.setNumeroPredialAntiguo(oferta.getNumeroPredialAntiguo());
        anteriorOferta.setCodigoHomologado(oferta.getCodigoHomologado());
        anteriorOferta.setMatriculaInmobiliaria(oferta.getMatriculaInmobiliaria());
        anteriorOferta.setCondicionJuridica(oferta.getCondicionJuridica());
        anteriorOferta.setTipoOferta(oferta.getTipoOferta());
        anteriorOferta.setOrigenOferta(oferta.getOrigenOferta());
        anteriorOferta.setTipoPredio(oferta.getTipoPredio());
        Oferta updatedOferta = service.save(anteriorOferta);
        
        return ResponseEntity.status(HttpStatus.CREATED).body(updatedOferta);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Optional<Oferta> oferta = service.findById(id);
        if(oferta.isPresent())
        {
            service.deleteById(id);
            return ResponseEntity.ok("Eliminado");
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }
}
