package com.igac.diegovillegas.test_igac.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.igac.diegovillegas.test_igac.models.CondicionJuridica;
import com.igac.diegovillegas.test_igac.services.CondicionJuridicaService;

@RestController
@RequestMapping("/api/condiciones-juridicas")
public class CondicionJuridicaController {

    @Autowired
    private CondicionJuridicaService service;

    @GetMapping("")
    public List<CondicionJuridica> list() {
        return service.findAll();
    }
}
