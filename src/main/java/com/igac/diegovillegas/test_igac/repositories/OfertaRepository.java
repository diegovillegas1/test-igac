package com.igac.diegovillegas.test_igac.repositories;

import org.springframework.data.repository.CrudRepository;

import com.igac.diegovillegas.test_igac.models.Oferta;

public interface OfertaRepository extends CrudRepository<Oferta, Long> {

}
